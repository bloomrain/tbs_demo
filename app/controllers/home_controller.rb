class HomeController < ApplicationController
  def index
    @player1 = Player::Neco.new(name: 'neco', id: 'player1')
    @player2 = Player::Neco.new(name: 'neco2', id: 'player2')
    @commands = Game.new(@player1, @player2).run!
  end
end
