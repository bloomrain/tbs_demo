class window.GamePreview
  constructor: (@commands)->
    @currentCommandIndex = 0
    @player1 = new Spearman('player1')
    @player2 = new Spearman('player2')

  run: ->
    timeOut = @runNextCommand()
    setTimeout(@run.bind(@), timeOut) if timeOut?

  endGame: ->
    $('.game-end').show()
    if @player1.dead
      $('.game-end').addClass('loose')
      $('.game-end').text('You died :(')
    else if @player2.dead
      $('.game-end').addClass('win')
      $('.game-end').text('You win!')
    else
      $('.game-end').addClass('tie')
      $('.game-end').text('Tie: nobody wins...')

    null

  runNextCommand: ->
    command = @commands[@currentCommandIndex]
    console.log command
    @currentCommandIndex += 1
    if command?
      @[command.id][command.action](command)
    else
      @endGame()
