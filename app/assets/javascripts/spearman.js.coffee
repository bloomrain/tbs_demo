class window.Spearman
  constructor: (name)->
    @$dom = $(".#{name}")
    @width = 80
    @height = 60
    @lastDirection = null

    framesPerAction = {
      attack: 8,
      move: 10,
      die: 8
    }
    for action in ['attack', 'move', 'die']
      for direction in ['up', 'down', 'left', 'right']
        @registerSprite("#{action}#{direction}", framesPerAction[action])

  start: (params)->
    @updateStats(params)
    top = (params.player.position[1] - 1) * @height
    left = (params.player.position[0] - 1) * @width
    @$dom.css('top': "#{ top }px", 'left': "#{ left }px")
    200

  wait: (params)->
    @updateStats(params)
    500

  attack: (params)->
    @updateStats(params)
    @makeAction("attack#{params.direction}", params)

  move: (params)->
    @updateStats(params)
    fullAction = "move#{params.direction}"
    timeOut = @makeAction(fullAction, params)
    left = (params.player.position[0] - 1) * @width
    top = (params.player.position[1] - 1) * @height
    @$dom.css('z-index': 100 + params.player.position[1])
    @$dom.animate({'left': "#{ left }px", 'top': "#{ top }px"}, timeOut)
    timeOut

  die: (params)->
    @updateStats(params)
    @makeAction("die#{@lastDirection}", params)

  win: (params)->
    @updateStats(params)

  makeAction: (action, params)->
    @lastDirection = params.direction if params.direction?
    @playAction(action)

    500

  updateStats: (params)->
    @position = params.player.position
    @dead = params.player.dead
    @health = params.player.health
    @name = params.player.name
    @$dom.find('.player-heath').css('width': "#{100 * @health / 50}%")

  switchToAction: (action)->
    @$dom.find('.player-action').hide()
    @$dom.find(".#{action}.player-action").show()

  playAction: (action)->
    @switchToAction(action)
    @$dom.find(".#{action}.player-action").animateSprite('restart')
    @$dom.find(".#{action}.player-action").animateSprite('play', action)

  showIdle: ->
    return if @dead

    @$dom.find('.player-action').hide()
    @$dom.find(".idle#{@lastDirection}.player-action").show()

  registerSprite: (action, frames)->
    animations = {}
    animations[action] = [1...frames]
    that = @

    @$dom.find(".#{action}.player-action").animateSprite({
      duration: 500,
      autoplay: false,
      loop: false,
      animations: animations,
      complete: -> that.showIdle()
    })
