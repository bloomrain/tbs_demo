require 'rails_helper'

describe Player::Neco do
  describe '#make_a_turn' do
    subject(:player) { Player::Neco.new(name: 'me', id: 'player1', enemy: enemy, position: position) }
    let(:enemy) { Player::Neco.new(name: 'enemy', id: 'player2', position: enemy_position) }
    let(:position) { [1, 1] }

    context 'when enemy is near' do
      let(:enemy_position) { [1, 2] }

      it 'attacks' do
        expect(player).to receive(:attack!)
        player.make_a_turn
      end
    end

    context 'when enemy is far away' do
      let(:enemy_position) { [1, 3] }

      it 'moves any where' do
        expect(player).to receive(:move!)
        player.make_a_turn
      end
    end
  end
end
