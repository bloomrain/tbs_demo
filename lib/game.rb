class Game
  MAX_TURNS = 100

  attr_reader :player, :other_player, :commands, :players_count, :turn_no

  def initialize(player, other_player)
    @player = player
    @other_player = other_player
    @players_count = 2
    @commands = []
  end

  def players
    @players ||= [player, other_player]
  end

  def setup_players!
    player.enemy = other_player
    commands << player.set_primary_position(1, 5)
    player.turns_queue = turns_queue.clone

    other_player.enemy = player
    other_player.turns_queue = turns_queue.clone
    commands << other_player.set_primary_position(9, 5)
  end

  def turns_queue
    @turns_queue ||= (MAX_TURNS * players.count).times.map do
      players.sample
    end
  end

  def run!
    setup_players!

    turns_queue.each.with_index do |active_player, turn_no|
      @turn_no = turn_no
      prepare_for_turn(active_player)
      commands << active_player.make_a_turn

      return end_game if players.detect(&:dead?).present?
    end
    commands
  end

  def end_game
    dead_player = players.detect(&:dead?)
    win_player = players.detect(&:live?)

    commands << dead_player.die
    commands << win_player.win

    commands
  end

  def prepare_for_turn(player)
    player.turn_number = turn_no
  end

end
