class BasePlayer
  attr_reader :name, :position, :health, :id
  attr_accessor :enemy, :turns_queue, :turn_number

  DIRECTION_MAP = {
    bottom: :down,
    top: :up
  }

  def initialize(name:, id:, enemy: nil, position: nil)
    @name = name
    @id = id
    @health = 50
    @enemy = enemy
    set_primary_position(*position.to_a) unless position.nil?
  end

  def stats
    {
      name: name,
      health: health,
      position: position.to_a,
      dead: dead?
    }
  end

  def set_primary_position(x, y)
    @position = Position.new([x, y])
    command('start')
  end

  def make_a_turn(turn_number)
  end

  def attack!(direction: :any)
    case direction.to_sym
    when :any
      return wait! if near_enemy_direction.nil?

      damage = 10
      enemy.receive_damage!

      command('attack', direction: near_enemy_direction.to_s, damage: damage)
    else
      fail "Unknown direction: #{direction}. Allowed directions are: :any, :left, :right, :up, :down, :bottom, :top"
    end

  end

  def can_attack_enemy?
    near_enemy_direction.present?
  end

  def receive_damage!
    @health -= 10
  end

  def dead?
    health <= 0
  end

  def live?
    !dead?
  end

  def die
    command('die')
  end

  def win
    command('win')
  end


  def wait!
    command('wait')
  end

  %i(left right up down bottom top).each do |direction|
    define_method "#{direction}" do
      move!(DIRECTION_MAP[direction] || direction)
    end
  end

  def move!(direction)
    next_position = position.to_a.clone

    case direction.to_sym
    when :right
      next_position[0] += 1
    when :left
      next_position[0] -= 1
    when :up
      next_position[1] -= 1
    when :down
      next_position[1] += 1
    else
      fail "Unknown direction: #{direction}. Allowed directions are: :any, :left, :right, :up, :down"
    end

    return bad_move_command(direction) if invalid_position?(next_position)

    position.x = next_position[0]
    position.y = next_position[1]

    command('move', direction: (DIRECTION_MAP[direction] || direction))
  end

  def invalid_position?(potential_position)
    potential_position = potential_position.to_a
    enemy.position.to_a == potential_position ||
      potential_position.min < 1 ||
      potential_position.max > 9
  end

  def near_enemy_direction
    if position.x - enemy.position.x == 0
      if position.y - enemy.position.y == 1
        :up
      elsif position.y - enemy.position.y == -1
        :down
      end
    elsif position.y - enemy.position.y == 0
      if position.x - enemy.position.x == 1
        :left
      elsif position.x - enemy.position.x == -1
        :right
      end
    end
  end

  private

  def bad_move_command(direction)
    command('wait', explanation: "wrong direction: #{direction}")
  end

  def command(action, params = {})
    { action: action, id: id, player: stats }.merge(params)
  end
end
