module Player
  class Neco < BasePlayer
    # available methods:
    #   #left!, #right!, #up!, #down!, #move(direction)
    #   #wait,
    #   #attack!, #can_attack_enemy?
    #   #turns_queue

    def make_a_turn
      if can_attack_enemy?
        attack!
      else
        move!(%i(left right up down).sample)
      end
    end
  end
end
